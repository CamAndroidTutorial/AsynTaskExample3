package com.bunhann.asyntaskexample3;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends AppCompatActivity {

    private Button btnInfo;
    private TextView lblData;

    private static double lat = 11.549852, lon = 104.9242594;
    private static String units = "imperial";
    private static final String APP_ID = "fa05e76386e286f64bc4486aab844a9c";
    private static String url = String.format("http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f&units=%s&appid=%s",
            lat, lon, units, APP_ID);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnInfo = (Button) findViewById(R.id.btnGetInfo);
        lblData = (TextView) findViewById(R.id.tvLabel);

        btnInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnInfo.setEnabled(false);
                new GetDataTask().execute(url);
            }
        });
    }

    private class GetDataTask extends AsyncTask<String, Void, String>{

        public GetDataTask() {
        }

        @Override
        protected String doInBackground(String... strings) {
            String weather = "UNDEFINED";
            try {
                URL url = new URL(strings[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream stream = new BufferedInputStream(urlConnection.getInputStream());
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
                StringBuilder builder = new StringBuilder();

                String inputString;
                while ((inputString = bufferedReader.readLine()) != null) {
                    builder.append(inputString);
                }

                JSONObject topLevel = new JSONObject(builder.toString());
                JSONObject main = topLevel.getJSONObject("main");
                double w = main.getDouble("temp");
                w = (w-32)*5/9;
                weather = String.valueOf(w);
                urlConnection.disconnect();

            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return weather;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            lblData.setText("PP Weather: " + s + " °C");
            btnInfo.setEnabled(true);
        }
    }
}
